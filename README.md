# Stockalike Station Parts Expansion Redux : UNOFFICIAL

This is an unofficial, non authorized repository for **Stockalike Station Parts Expansion Redux** for historical reference and troubleshooting.


## In a Hurry
* [Binaries](https://github.com/net-lisias-ksph/StationPartsExpansionRedux/tree/ARR/Archive)
* [Sources](https://github.com/net-lisias-ksph/StationPartsExpansionRedux/tree/ARR/Master)
* [Change Log](./CHANGE_LOG.md)


## Description

A complete and total rebuild of my old Stockalike Station Parts Expansion project. Everything has been redone, and nothing is left of the old mod! Check out some key features:

* Small Station Parts:  A full set of 1.25m station parts, with habitats, control rooms, connecting tubes, hubs and much more.
* Medium Station Parts: Expanding the 2.5m set that stock provides, you'll find greenhouses, structural components, specialised connectors, orbital stowage bays...
* Large Station Parts:  Yup, large station parts! A full set in the 3.75m size class, from habs to labs to... well, lots more!
* Reworks: I've taken the time to rework the three stock station components (Cupola, MPL and Hitchhiker) to match the new parts. They look the same but new!
* Inflatable Habitats:  A nice set of inflatable habitats for maximum crew space. They range from teeny to huge.
* Centrifuges: All size categories have at least one centrifuge habitat. They range from small and cramped to massive and spacious (one of the largest parts I've ever made).
* Cargo Containers: I really felt like making some multi-purpose cargo containers. These modules will adapt to what mods you have installed, and provide storage for resources from MKS, TAC-LS, USI-LS, EPL and probably a few more that I forgot. 
* Self-Levelling Base Frames: To help align your bases on slightly bumpy terrain, use these base plates, which feature individually adjustable legs with a self-level function
* Extensible Crew Connections: Much like the Klaw, but better! Varying lengths and impressive looks!
* Comprehensive Mod Support: With the help of many forumgoers, this mod contains support for tons of mods, specifically those related to life support and colonisation.
* IVAs: It almost killed me but everything has fully featured IVAs. 

Full integration with all my other mods. Community Tech Tree support. What more could you ask for? Actually, don't answer. 

### Dependencies

* Optional
	+ [RasterPropMonitor](https://forum.kerbalspaceprogram.com/index.php?/topic/105821-16x-rasterpropmonitor-development-stopped-v0306-29-december-2018/)


## License

This AddOn is (C) [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/). All Right Reserved.

This repository is under the claim of the [right to backup](https://info.legalzoom.com/copyright-law-making-personal-copies-22200.html) and should be considered private:

> Copyright law permits you to make one copy of your computer software for the purpose of archiving the software in case it is damaged or lost. In order to make a copy, you must own a valid copy of the software and must destroy or transfer your backup copy if you ever decide to sell, transfer or give away your original valid copy. It is not legal to sell a backup copy of software unless you are selling it along with the original.

I grant you no rights on any artefact on this repository, unless you own yourself the right to use the software and authorises me to keep backups for you:

> (a) Making of Additional Copy or Adaptation by Owner of Copy. -- Notwithstanding the provisions of section 106, it is not an infringement for the owner of a copy of a computer program to make or authorise the making of another copy or adaptation of that computer program provided:
> 
>> (2) that such new copy or adaptation is for archival purposes only and that all archival copies are destroyed in the event that continued possession of the computer program should cease to be rightful.

[17 USC §117(a)](https://www.law.cornell.edu/uscode/text/17/117)

By using this repository without the required pre-requisites, I hold you liable to copyright infringement.


## References

* [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/)
	+ [KSP Forum](https://forum.kerbalspaceprogram.com/index.php?/topic/170211-*/)
	+ [CurseForge](https://www.curseforge.com/kerbal/ksp-mods/stockalike-station-parts-expansion-redux/)
	+ [SpaceDock](https://spacedock.info/mod/1682/Stockalike%20Station%20Parts%20Expansion%20Redux)
	+ [GitHub](https://github.com/post-kerbin-mining-corporation/StationPartsExpansionRedux)
